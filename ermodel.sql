/*
 * Modelo de base de datos de LA MEJOR COCINA
 */

drop table if exists detalle_factura;
drop table if exists factura;
drop table if exists cocinero;
drop table if exists camarero;
drop table if exists mesa;
drop table if exists cliente;

-- Table Cliente
create table cliente
(
  id_cliente    bigserial   not null,
  nombre        varchar(30) not null,
  apellido1     varchar(25) not null,
  apellido2     varchar(25),
  observaciones varchar(500),
  constraint pk_id_cliente primary key (id_cliente)
);

-- Table Mesa
create table mesa
(
  id_mesa            bigserial   not null,
  num_max_comensales smallint    not null,
  ubicacion          varchar(50) not null,
  constraint pk_id_mesa primary key (id_mesa)
);

-- Table Camarero
create table camarero
(
  id_camarero bigserial   not null,
  nombre      varchar(30) not null,
  apellido1   varchar(25) not null,
  apellido2   varchar(25),
  constraint pk_id_camarero primary key (id_camarero)
);

-- Table Cocinero
create table cocinero
(
  id_cocinero bigserial   not null,
  nombre      varchar(30) not null,
  apellido1   varchar(25) not null,
  apellido2   varchar(25),
  constraint pk_id_cocinero primary key (id_cocinero)
);

-- Table Factura
create table factura
(
  id_factura    bigserial not null,
  id_cliente    bigint    not null,
  id_camarero   bigint    not null,
  id_mesa       bigint    not null,
  fecha_factura date      not null default now(),
  constraint pk_id_factura primary key (id_factura),
  constraint fk_id_cliente foreign key (id_cliente) references cliente (id_cliente),
  constraint fk_id_camarero foreign key (id_camarero) references camarero (id_camarero),
  constraint fk_id_mesa foreign key (id_mesa) references mesa (id_mesa)
);

-- Table DetalleFactura
create table detalle_factura
(
  id_detalle_factura bigserial not null,
  id_factura         bigint    not null,
  id_cocinero        bigint    not null,
  plato              varchar(100),
  importe            float,
  constraint pk_id_detalle_factura primary key (id_detalle_factura),
  constraint fk_id_factura foreign key (id_factura) references factura (id_factura),
  constraint fk_id_cocinero foreign key (id_cocinero) references cocinero (id_cocinero)
);