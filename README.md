# LA MEJOR COCINA
Prueba de conocimientos para Conexia

## Especificaciones
* Aplicación web desarrollada en Spring Boot.
* Persistencia de datos con JAP (Hibernate).
* Base de datos PostgreSQL (archivo `/ermodel.sql`).
* Usando principio de diseño de software IoC.
* Desarrollo con servicios REST.
* en `/public/` están los archivos web.

## Requerimientos para ejecución
* Java EE 7

## Instrucciones para la ejecución
Las aplicaciones Spring Boot funcionan como un empaquetado completo por defecto,
por lo que no se requiere de un servidor de aplicaciones y ejecuta Apache Tomcat por defecto.

1. Hacer la instalación del archivo ermodel.sql en un servidor de bases de datos PostgreSQL.
2. La configuración de la conexión de base de datos se hace en el archivo `/src/main/resources/application.yml`.
```yaml
spring:
  application:
    name: LA MEJOR COCINA

  datasource:
    url: ${JDBC_DATABASE_URL} # jdbc:postgresql://localhost:5432/postgres
    username: ${JDBC_DATABASE_USERNAME} # cualquiera
    password: ${JDBC_DATABASE_PASSWORD} # cualquiera
```
3. En la raíz del proyecto ejecutar el comando:
```bash
$ ./mvnw spring-boot:run
```

## Ejecución directa de archivo generado .jar
1. Solo ejecutar el comando:
```bash
$ java -jar target/conexia-1.0.1.jar
```

## Demo en vivo (heroku)
* https://prueba-conexia.herokuapp.com/
