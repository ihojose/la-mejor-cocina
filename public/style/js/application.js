angular.module('conexia', [])
    .controller('hacerorden', ['$scope', '$http', '$timeout', ($scope, $http, $timeout) => {
        $scope.platos = [{
            plato: '',
            cocinero: null,
            importe: 0
        }];

        $scope.cocineros = [];
        $scope.camareros = [];
        $scope._camareros = [];
        $scope.mesas = [];
        $scope.sumatoria = 0;
        $scope.mes = moment();

        $scope.loading = false;

        $scope._cliente = {
            nombre: '',
            apellido1: '',
            apellido2: '',
            observaciones: ''
        };

        $scope._orden = {
            cliente: null,
            camarero: null,
            mesa: null,
            fecha: moment().format('YYYY-MM-DD'),
            detalles: $scope.platos
        };

        $scope.$watch('mes', newVal => {
            $http({
                url: `/camarero/${moment(newVal).format('YYYY')}/${moment(newVal).format('MM')}`,
                method: 'GET'
            }).then(response => {
                $scope._camareros = response.data;
            }, error => {
                UIkit.notification('Hubo un error inesperado', {status: 'danger'});
            });
        });

        $scope.getTop = () => $http({
            url: '/cliente/top',
            method: 'GET'
        }).then(response => {
            $scope._tops = response.data;
        }, error => {
            UIkit.notification('Hubo un error inesperado', {status: 'danger'});
        });

        $scope.sumar = () => {
            $scope.sumatoria = 0;
            for (let i of $scope.platos) {
                $scope.sumatoria += i.importe;
            }
        };

        $scope.agregarPlato = () => {
            $scope.platos.push({
                plato: '',
                cocinero: null,
                importe: 0
            });
        };

        $scope.crearOrden = () => {
            $scope.loading = true;
            $http({
                url: '/cliente',
                method: 'POST',
                data: $scope._cliente
            }).then(response => {
                $scope._orden.cliente = response.data.id;
                $http({
                    url: '/factura',
                    method: 'POST',
                    data: $scope._orden
                }).then(_res => {
                    $scope.loading = false;
                    UIkit.notification('Orden registrada', {status: 'success'});

                    $scope._orden = {
                        cliente: null,
                        camarero: null,
                        mesa: null,
                        fecha: moment().format('YYYY-MM-DD'),
                        detalles: $scope.platos
                    };
                }, _error => {
                    $scope.loading = false;
                    UIkit.notification('Hubo un error inesperado', {status: 'danger'});
                });
            }, error => {
                UIkit.notification('Hubo un error inesperado', {status: 'danger'});
                $scope.loading = false;
            });
        };

        // ON start
        $scope.onStart = () => {
            $http({
                url: '/cocinero',
                method: 'GET'
            }).then(response => {
                $scope.cocineros = response.data;
            }, error => {
                UIkit.notification('Hubo un error inesperado', {status: 'danger'});
            });

            $http({
                url: '/camarero',
                method: 'GET'
            }).then(response => {
                $scope.camareros = response.data;
            }, error => {
                UIkit.notification('Hubo un error inesperado', {status: 'danger'});
            });

            $http({
                url: '/mesa',
                method: 'GET'
            }).then(response => {
                $scope.mesas = response.data;
            }, error => {
                UIkit.notification('Hubo un error inesperado', {status: 'danger'});
            });
        };

        $scope.onStart();
    }]);