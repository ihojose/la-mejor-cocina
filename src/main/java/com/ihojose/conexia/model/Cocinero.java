package com.ihojose.conexia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "cocinero")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@EqualsAndHashCode
public class Cocinero {
    @Id
    @GeneratedValue(generator = "generador_cocinero")
    @SequenceGenerator(name = "generador_cocinero", sequenceName = "secuencia_cocinero", allocationSize = 1)
    @Column(name = "id_cocinero")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido1")
    private String apellido1;

    @Column(name = "apellido2")
    private String apellido2;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cocinero")
    @JsonIgnore
    private Set<DetalleFactura> detalles;
}
