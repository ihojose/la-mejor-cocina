package com.ihojose.conexia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "detalle_factura")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class DetalleFactura {
    @Id
    @GeneratedValue(generator = "detalle_generador")
    @SequenceGenerator(name = "detalle_generador", sequenceName = "secuencia_detalle", allocationSize = 1)
    @Column(name = "id_detalle_factura")
    private Long id;

    @Column(name = "plato")
    private String plato;

    @Column(name = "importe")
    private Double importe;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Factura.class)
    @JoinColumn(name = "id_factura", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Factura factura;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Cocinero.class)
    @JoinColumn(name = "id_cocinero", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Cocinero cocinero;
}
