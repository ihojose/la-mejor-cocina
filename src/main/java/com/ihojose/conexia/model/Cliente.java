package com.ihojose.conexia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "cliente")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Cliente {
    @Id
    @GeneratedValue(generator = "generador_cliente")
    @SequenceGenerator(name = "generador_cliente", sequenceName = "secuencia_cliente", allocationSize = 1)
    @Column(name = "id_cliente")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido1")
    private String apellido1;

    @Column(name = "apellido2")
    private String apellido2;

    @Column(name = "observaciones")
    private String observaciones;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
    @JsonIgnore
    private Set<Factura> facturas;
}
