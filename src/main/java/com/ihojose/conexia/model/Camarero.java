package com.ihojose.conexia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "camarero")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Camarero {
    @Id
    @GeneratedValue(generator = "generador_camarero")
    @SequenceGenerator(name = "generador_camarero", sequenceName = "secuancia_camarero", allocationSize = 1)
    @Column(name = "id_camarero")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido1")
    private String apellido1;

    @Column(name = "apellido2")
    private String apellido2;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "camarero")
    @JsonIgnore
    private Set<Factura> factura;
}
