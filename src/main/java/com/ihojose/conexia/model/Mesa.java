package com.ihojose.conexia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "mesa")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Mesa {
    @Id
    @GeneratedValue(generator = "generador_mesa")
    @SequenceGenerator(name = "generador_mesa", sequenceName = "secuancia_mesa", allocationSize = 1)
    @Column(name = "id_mesa")
    private Long id;

    @Column(name = "num_max_comensales")
    private Integer numMaxComensales;

    @Column(name = "ubicacion")
    private String ubicacion;

    @OneToMany(mappedBy = "mesa", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Factura> facturas;
}
