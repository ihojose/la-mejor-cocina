package com.ihojose.conexia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "factura")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@EqualsAndHashCode
public class Factura {
    @Id
    @GeneratedValue(generator = "factura_generador")
    @SequenceGenerator(name = "factura_generador", sequenceName = "secuencia_factura", allocationSize = 1)
    @Column(name = "id_factura")
    private Long id;

    @Column(name = "fecha_factura")
    private LocalDate fechaFactura;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Cliente.class)
    @JoinColumn(name = "id_cliente", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Cliente cliente;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Camarero.class)
    @JoinColumn(name = "id_camarero", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Camarero camarero;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Mesa.class)
    @JoinColumn(name = "id_mesa", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Mesa mesa;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "factura")
    @JsonIgnore
    private Set<DetalleFactura> detalles;
}
