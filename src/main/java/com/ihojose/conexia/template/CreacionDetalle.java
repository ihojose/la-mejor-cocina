package com.ihojose.conexia.template;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreacionDetalle {
    private String plato;
    private Double importe;
    private Long cocinero;
}
