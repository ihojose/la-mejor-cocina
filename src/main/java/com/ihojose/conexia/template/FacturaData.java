package com.ihojose.conexia.template;

import com.ihojose.conexia.model.DetalleFactura;
import com.ihojose.conexia.model.Factura;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FacturaData {
    private Factura informacion;
    private List<DetalleFactura> detalles;
}
