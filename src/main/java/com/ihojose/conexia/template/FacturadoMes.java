package com.ihojose.conexia.template;

import com.ihojose.conexia.model.Camarero;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FacturadoMes {
    private Camarero camarero;
    private Long totalFacturado;
}
