package com.ihojose.conexia.template;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SumatoriaMensual {
    private String nombre;
    private String apellido;
    private String mes;
    private String faturado;
}
