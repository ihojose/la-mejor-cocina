package com.ihojose.conexia.template;

import com.ihojose.conexia.model.Cliente;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TopCliente {
    private Cliente cliente;
    private Long gastado;
}
