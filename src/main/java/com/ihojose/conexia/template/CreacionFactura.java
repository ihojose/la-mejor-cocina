package com.ihojose.conexia.template;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class CreacionFactura {
    private String fecha;
    private Long cliente;
    private Long camarero;
    private Long mesa;
    private Set<CreacionDetalle> detalles;
}
