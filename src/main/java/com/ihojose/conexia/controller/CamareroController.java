package com.ihojose.conexia.controller;

import com.ihojose.conexia.model.Camarero;
import com.ihojose.conexia.model.DetalleFactura;
import com.ihojose.conexia.model.Factura;
import com.ihojose.conexia.repository.CamareroRepository;
import com.ihojose.conexia.repository.DetalleFacturaRepository;
import com.ihojose.conexia.repository.FacturaRepository;
import com.ihojose.conexia.template.FacturaData;
import com.ihojose.conexia.template.FacturadoMes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@RequestMapping(path = "/camarero", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class CamareroController {
    private static final Logger LOG = Logger.getLogger(CamareroController.class.getSimpleName());

    private final CamareroRepository camarero;
    private final FacturaRepository factura;
    private final DetalleFacturaRepository detalle;

    @Autowired
    public CamareroController(CamareroRepository camarero, FacturaRepository factura, DetalleFacturaRepository detalle) {
        this.camarero = camarero;
        this.factura = factura;
        this.detalle = detalle;
    }

    /**
     * Traer todos los camareros
     *
     * @return lista
     */
    @GetMapping
    public List<Camarero> camareros() {
        return camarero.findAll();
    }

    /**
     * Obtener un camamero por id
     *
     * @param id id camarero
     * @return entidad camarero
     */
    @GetMapping(value = "/{id}")
    public Optional<Camarero> getById(@PathVariable("id") Long id) {
        return camarero.findById(id);
    }

    /**
     * Obtener facturado por camarero.
     *
     * @param anno año
     * @param mes  mes
     * @return facturado mensual
     */
    @GetMapping(value = "/{anno}/{mes}")
    public List<FacturadoMes> facturadoMensual(@PathVariable("anno") Integer anno, @PathVariable("mes") Integer mes) {
        List<FacturadoMes> dres = new ArrayList<>();

        for (Camarero ca : camarero.findAll()) {
            List<Factura> facturas = factura.encontrarPorCamarero(ca.getId());
            long total = 0;
            for (Factura factura : facturas) {
                LocalDate f = factura.getFechaFactura();

                if (f.getYear() == anno && f.getMonthValue() == mes) {
                    for (DetalleFactura det : detalle.encontrarPorFactura(factura.getId())) {
                        total += det.getImporte();
                    }
                }
            }

            dres.add(new FacturadoMes(ca, total));
        }

        return dres;
    }
}
