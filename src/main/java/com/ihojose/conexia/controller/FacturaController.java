package com.ihojose.conexia.controller;

import com.ihojose.conexia.model.DetalleFactura;
import com.ihojose.conexia.model.Factura;
import com.ihojose.conexia.repository.*;
import com.ihojose.conexia.template.CreacionDetalle;
import com.ihojose.conexia.template.CreacionFactura;
import com.ihojose.conexia.template.FacturaData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(path = "factura", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class FacturaController {
    private static final Logger LOG = Logger.getLogger(FacturaController.class.getSimpleName());

    private final FacturaRepository factura;
    private final ClienteRepository cliente;
    private final CamareroRepository camarero;
    private final MesaRepository mesa;
    private final DetalleFacturaRepository detalle;
    private final CocineroRepository cocineero;

    @Autowired
    public FacturaController(FacturaRepository factura, ClienteRepository cliente, CamareroRepository camarero, MesaRepository mesa, DetalleFacturaRepository detalle, CocineroRepository cocineero) {
        this.factura = factura;
        this.cliente = cliente;
        this.camarero = camarero;
        this.mesa = mesa;
        this.detalle = detalle;
        this.cocineero = cocineero;
    }

    /**
     * Mostrar todas las facturas
     *
     * @return lista de facturas
     */
    @GetMapping
    public List<Factura> facturas() {
        return factura.findAll();
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @GetMapping(value = "/{id}")
    public FacturaData factura(@PathVariable("id") Long id) {
        Factura f = factura.findById(id).get();

        return new FacturaData(f, detalle.encontrarPorFactura(id));
    }

    /**
     * Nueva factura a guardar
     *
     * @param f factura
     * @return error o exito
     */
    @SuppressWarnings({"OptionalGetWithoutIsPresent"})
    @PostMapping
    public ResponseEntity<Factura> crear(@RequestBody CreacionFactura f) {
        try {
            Factura fd = new Factura();
            fd.setFechaFactura(LocalDate.parse(f.getFecha()));
            fd.setCliente(cliente.findById(f.getCliente()).get());
            fd.setCamarero(camarero.findById(f.getCamarero()).get());
            fd.setMesa(mesa.findById(f.getMesa()).get());

            Factura result = factura.save(fd);

            for (CreacionDetalle d : f.getDetalles()) {
                DetalleFactura dd = new DetalleFactura();
                dd.setPlato(d.getPlato());
                dd.setImporte(d.getImporte());
                dd.setCocinero(cocineero.findById(d.getCocinero()).get());
                dd.setFactura(result);

                detalle.save(dd);
            }

            return new ResponseEntity<>(fd, HttpStatus.CREATED);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "RequestError({0})", factura.toString());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}
