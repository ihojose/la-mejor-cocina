package com.ihojose.conexia.controller;

import com.ihojose.conexia.model.Cocinero;
import com.ihojose.conexia.repository.CocineroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping( path = "/cocinero", produces = MediaType.APPLICATION_JSON_UTF8_VALUE )
public class CocineroController {
    private static final Logger LOG = Logger.getLogger( CocineroController.class.getSimpleName() );

    /**
     * Repositorio de base de datos de cocineros
     */
    private final CocineroRepository cocineros;

    /**
     * Persistencia de cocineros como Inversion de Control (IoC)
     *
     * @param cocineros ConcinerosRepository
     */
    @Autowired
    public CocineroController( CocineroRepository cocineros ) {
        this.cocineros = cocineros;
    }

    /**
     * Devolver todos los cocineros en lista básica para la prueba
     *
     * @return Lista de concineros como una lista
     */
    @GetMapping
    public List<Cocinero> getCocineros() {
        return cocineros.findAll();
    }
}
