package com.ihojose.conexia.controller;

import com.ihojose.conexia.model.Mesa;
import com.ihojose.conexia.repository.MesaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping( path = "/mesa", produces = MediaType.APPLICATION_JSON_UTF8_VALUE )
public class MesaController {
    private static final Logger LOG = Logger.getLogger( MesaController.class.getSimpleName() );

    private final MesaRepository mesa;

    @Autowired
    public MesaController( MesaRepository mesa ) {
        this.mesa = mesa;
    }

    /**
     * Ver todas las mesas
     *
     * @return lista de mesas
     */
    @GetMapping
    public List<Mesa> mesas() {
        return mesa.findAll();
    }

    /**
     * Crear mesa
     *
     * @param d data
     * @return exito o error
     */
    @PostMapping
    public ResponseEntity crear( @RequestBody Mesa d ) {
        try {
            mesa.save( d );

            return new ResponseEntity( HttpStatus.CREATED );
        } catch ( Exception e ) {
            LOG.log( Level.SEVERE, "RequestError({0}) ", d.toString() );
            throw new ResponseStatusException( HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage() );
        }
    }
}
