package com.ihojose.conexia.controller;

import com.ihojose.conexia.model.Cliente;
import com.ihojose.conexia.model.DetalleFactura;
import com.ihojose.conexia.model.Factura;
import com.ihojose.conexia.repository.ClienteRepository;
import com.ihojose.conexia.repository.DetalleFacturaRepository;
import com.ihojose.conexia.repository.FacturaRepository;
import com.ihojose.conexia.template.TopCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(path = "/cliente", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ClienteController {
    private static final Logger LOG = Logger.getLogger(ClienteController.class.getSimpleName());

    /**
     * Repositorio de base de datos
     */
    private final ClienteRepository cliente;
    private final FacturaRepository factura;
    private final DetalleFacturaRepository detalle;

    /**
     * Persistencia de cliente como Inversion de Control (IoC)
     *
     * @param cliente ConcinerosRepository
     * @param factura
     * @param detalle
     */
    @Autowired
    public ClienteController(ClienteRepository cliente, FacturaRepository factura, DetalleFacturaRepository detalle) {
        this.cliente = cliente;
        this.factura = factura;
        this.detalle = detalle;
    }

    /**
     * Todos los clientes
     *
     * @return lista
     */
    @GetMapping
    public List<Cliente> getClientes() {
        return cliente.findAll();
    }

    /**
     * Obtener cliente por id
     *
     * @param id id cliente
     * @return objeto de cliente
     */
    @GetMapping(value = "/{id}")
    public Optional<Cliente> getById(@PathVariable("id") Long id) {
        return cliente.findById(id);
    }

    /**
     * Crear cliente
     *
     * @param data cliente
     * @return exito o error
     */
    @PostMapping
    public ResponseEntity<Cliente> crear(@RequestBody Cliente data) {
        try {
            Cliente c = cliente.save(data);
            return new ResponseEntity<>(c, HttpStatus.CREATED);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "RequestError({0})", data.toString());
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, e.getMessage());
        }
    }

    /**
     * Actualizar a un cliente
     *
     * @param data cliente
     * @return exito o error
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity actualizar(@PathVariable("id") Long id, @RequestBody Cliente data) {
        try {
            Cliente c = cliente.getOne(id);
            c.setNombre(data.getNombre());
            c.setApellido1(data.getApellido1());
            c.setApellido2(data.getApellido2());
            c.setObservaciones(data.getObservaciones());
            cliente.save(c);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "RequestError({0})", data.toString());
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, e.getMessage());
        }
    }

    @GetMapping(path = "/top")
    public List<TopCliente> topClientes() {
        List<TopCliente> dres = new ArrayList<>();

        for (Cliente c : cliente.findAll()) {
            List<Factura> facturas = factura.encontrarPorCliente(c.getId());
            long total = 0;

            for (Factura factura : facturas) {
                for (DetalleFactura det : detalle.encontrarPorFactura(factura.getId())) {
                    total += det.getImporte();
                }
            }

            if (total >= 100000l) dres.add(new TopCliente(c, total));
        }

        return dres;
    }
}
