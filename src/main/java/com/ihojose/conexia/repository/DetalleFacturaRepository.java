package com.ihojose.conexia.repository;

import com.ihojose.conexia.model.DetalleFactura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface DetalleFacturaRepository extends JpaRepository<DetalleFactura, Long> {
    @Query(value = "select * from detalle_factura where id_factura = ?1", nativeQuery = true)
    List<DetalleFactura> encontrarPorFactura(Long factura);
}
