package com.ihojose.conexia.repository;

import com.ihojose.conexia.model.Cocinero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CocineroRepository extends JpaRepository<Cocinero, Long> {
}
