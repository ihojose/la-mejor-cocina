package com.ihojose.conexia.repository;

import com.ihojose.conexia.model.Factura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FacturaRepository extends JpaRepository<Factura, Long> {
    @Query(value = "select * from factura where id_camarero = ?1", nativeQuery = true)
    List<Factura> encontrarPorCamarero(Long id);

    @Query(value = "select * from factura where id_cliente = ?1", nativeQuery = true)
    List<Factura> encontrarPorCliente(Long id);
}
