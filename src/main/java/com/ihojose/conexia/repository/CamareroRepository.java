package com.ihojose.conexia.repository;

import com.ihojose.conexia.model.Camarero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CamareroRepository extends JpaRepository<Camarero, Long> {
}
